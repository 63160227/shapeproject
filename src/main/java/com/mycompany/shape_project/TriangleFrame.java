/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shape_project;

/**
 *
 * @author
 */

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author
 */
public class TriangleFrame extends JFrame {

    JLabel lblbase;
    JLabel lblHeight;
    JTextField txtbase;
    JTextField txtHeight;
    JButton btnCalculate;
    JLabel lblResult;

    public TriangleFrame() {
        super("Triangle");
        this.setSize(500, 250);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setLayout(null);


        lblbase = new JLabel("Base", JLabel.TRAILING);
        lblbase.setSize(50, 20);
        lblbase.setLocation(5, 5);
        lblbase.setBackground(Color.WHITE);
        lblbase.setOpaque(true);
        this.add(lblbase);

        lblHeight = new JLabel("Height", JLabel.TRAILING);
        lblHeight.setSize(55, 85);
        lblHeight.setLocation(5, 5);
        lblHeight.setBackground(Color.WHITE);
        lblHeight.setOpaque(true);
        this.add(lblHeight);

        txtbase = new JTextField();
        txtbase.setSize(100, 20);
        txtbase.setLocation(70, 5);
        this.add(txtbase);

        txtHeight = new JTextField();
        txtHeight.setSize(100, 20);
        txtHeight.setLocation(70, 40);
        this.add(txtHeight);

        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(250, 5);
        this.add(btnCalculate);

        lblResult = new JLabel("base = ??? Height = ??? area = ??? perimeter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(400, 50);
        lblResult.setLocation(0, 100);
        lblResult.setBackground(Color.WHITE);
        lblResult.setOpaque(true);
        this.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strBase = txtbase.getText();
                    double base = Double.parseDouble(strBase);
                    String strHeight = txtHeight.getText();
                    double height = Double.parseDouble(strHeight);
                    Triangle triangle = new Triangle(base, height);
                    lblResult.setText("Base = " + String.format("%.2f", triangle.getB())
                            + "Height = " + String.format("%.2f", triangle.getH())
                            + " area = " + String.format("%.2f", triangle.calArea())
                            + " perimeter = " + String.format("%.2f", triangle.calPerimeter()));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(TriangleFrame.this, "Error: Please input number",
                            "Errror", JOptionPane.ERROR_MESSAGE);
                    txtbase.setText("");
                    txtbase.requestFocus();
                    txtHeight.setText("");
                    txtHeight.requestFocus();
                }
            }
        }
        );
        this.setVisible(true);
    }

    public static void main(String[] args) {
        TriangleFrame frame = new TriangleFrame();
        frame.setVisible(true);
    }

}

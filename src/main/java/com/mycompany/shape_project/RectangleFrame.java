/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shape_project;

/**
 *
 * @author
 */
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author
 */
public class RectangleFrame extends JFrame {

    JLabel lblWidth;
    JLabel lblHeight;
    JTextField txtWidth;
    JTextField txtHeight;
    JButton btnCalculate;
    JLabel lblResult;

    public RectangleFrame() {
        super("Rectangle");
        this.setSize(400, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        lblWidth = new JLabel("Width", JLabel.TRAILING);
        lblWidth.setSize(50, 20);
        lblWidth.setLocation(5, 5);
        lblWidth.setBackground(Color.WHITE);
        lblWidth.setOpaque(true);
        this.add(lblWidth);

        lblHeight = new JLabel("Height", JLabel.TRAILING);
        lblHeight.setSize(55, 85);
        lblHeight.setLocation(5, 5);
        lblHeight.setBackground(Color.WHITE);
        lblHeight.setOpaque(true);
        this.add(lblHeight);

        txtWidth = new JTextField();
        txtWidth.setSize(100, 20);
        txtWidth.setLocation(70, 5);
        this.add(txtWidth);

        txtHeight = new JTextField();
        txtHeight.setSize(100, 20);
        txtHeight.setLocation(70, 40);
        this.add(txtHeight);

        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(250, 5);
        this.add(btnCalculate);

        lblResult = new JLabel("Width = ??? Height = ??? area = ??? perimeter = ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(400, 50);
        lblResult.setLocation(0, 100);
        lblResult.setBackground(Color.WHITE);
        lblResult.setOpaque(true);
        this.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String strWidth = txtWidth.getText();
                    double width = Double.parseDouble(strWidth);
                    String strHeight = txtHeight.getText();
                    double height = Double.parseDouble(strHeight);
                    Rectangle rectangle = new Rectangle(width, height);
                    lblResult.setText("Width = " + String.format("%.2f", rectangle.getW())
                            + "Height = " + String.format("%.2f", rectangle.getH())
                            + " area = " + String.format("%.2f", rectangle.calArea())
                            + " perimeter = " + String.format("%.2f", rectangle.calPerimeter()));

                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(RectangleFrame.this, "Error: Please input number",
                            "Errror", JOptionPane.ERROR_MESSAGE);
                    txtWidth.setText("");
                    txtWidth.requestFocus();
                    txtHeight.setText("");
                    txtHeight.requestFocus();
                }

            }
        }
        );

        this.setVisible(true);
    }

    public static void main(String[] args) {
        RectangleFrame frame = new RectangleFrame();
        frame.setVisible(true);
    }

}

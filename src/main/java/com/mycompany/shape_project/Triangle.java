/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shape_project;

/**
 *
 * @author a
 */
public class Triangle extends Shape {

    private double h;
    private double b;

    public Triangle(double h, double b) {
        super("Triangle");
        this.h = h;
        this.b = b;
    }

    public double getH() {
        return h;
    }

    public void setH(double h) {
        this.h = h;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }

    @Override
    public double calArea() {
        return (h * b) / 2;
    }

    @Override
    public double calPerimeter() {
        double c = Math.sqrt((Math.pow(h, 2) + Math.pow(b, 2)));
        return c + h + b;
    }
}
